package RoadRepair;
import java.io.*;
import java.util.*;



class Result {

    /*
     * Complete the 'getMinCost' function below.
     *
     * The function is expected to return a LONG_INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY crew_id
     *  2. INTEGER_ARRAY job_id
     */

    public static long getMinCost(List<Integer> crew_id, List<Integer> job_id) {
        long res = 0;
        // crew_id.sort();
        Collections.sort(crew_id);
        Collections.sort(job_id);
        // Collections.reverse(crew_id);
        
        for (int i = 0; i < crew_id.size(); i++) {
            if(job_id.get(i)>crew_id.get(i)){
                res += (job_id.get(i)-crew_id.get(i));   
            }else{
                res += (crew_id.get(i)-job_id.get(i));
            }            
        }        
    return res;


    }

}

public class RoadRepair {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Integer> crew_id = new ArrayList<Integer>();
        List<Integer> job_id = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            crew_id.add(sc.nextInt());
        }
        for (int i = 0; i < n; i++) {
            job_id.add(sc.nextInt());
        }
        sc.close();
        System.out.print(Result.getMinCost(crew_id, job_id));

    }
}
