package UnexpectedDemand;
import java.io.*;
import java.util.*;



class Result {

    /*
     * Complete the 'filledOrders' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY order
     *  2. INTEGER k
     */

    public static int filledOrders(List<Integer> order, int k) {
        Collections.sort(order);
        int res = 0;
        for (int i = 0; i < order.size(); i++) {
            if (order.get(i)<=k) {
                res++;
                k -= order.get(i);
            }
        }
        return res;

    }

}

public class UnexpectedDemand {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        List<Integer> order = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            order.add(sc.nextInt());
        }
        int k = sc.nextInt();
        sc.close();
        System.out.print(Result.filledOrders(order, k));        
    }
}
